# soal-shift-sisop-modul-4-E01-2022
Repo untuk soal shift sisop modul 4


## DAFTAR ISI ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomor 1](#nomor-1)
    - [Soal  1.a](#soal-1a)
    - [Soal  1.b](#soal-1b)
    - [Soal  1.c](#soal-1c)
    - [Soal  1.d](#soal-1d)
    - [Soal  1.e](#soal-1e)
- [Nomor 2](#nomor-2)
    - [Soal  2.a](#soal-2a)
    - [Soal  2.b](#soal-2b)
    - [Soal  2.c](#soal-2c)
    - [Soal  2.d](#soal-2d)
    - [Soal  2.e](#soal-2e)
- [Nomor 3](#nomor-3)
    - [Soal  3.a](#soal-3a)
    - [Soal  3.b](#soal-3b)
    - [Soal  3.c](#soal-3c)
    - [Soal  3.d](#soal-3d)
    - [Soal  3.e](#soal-3e)
- [Dokumentasi](#dokumentasi)
## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201237  | Putu Ravindra Wiguna | SISOP E
5025201003    | Rahmat Faris Akbar | SISOP E
05111740000146    | Muhammad Kiantaqwa Farhan | SISOP E

## Nomor 1 ##
> Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:


### Soal 1.a ###
> Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
> 
> Contoh : 
> 	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”



Pertama, kita harus membuat encoder dan decoder untuh path
#### Dokumentasi Source Code ####

```bash
void traverseEncode(char *path){
    DIR *dp;
    struct dirent *ep;
    char *fpath = malloc(sizeof(char)*512);
    // check if relative path is an absolute path (a.k.a include dirpath)
    if(strncmp(path, dirpath, strlen(dirpath))==0){
        strcpy(fpath, path);
    }else{
        // hm possibly not an absolute path, lets make it absolute
        sprintf(fpath, "%s%s",dirpath,path);
        // free(fpath);// this surely wont affect?  nah better comment it for sure
    }
    // Fpath should be a full path sir
    dp = opendir(fpath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                // not a hidden file, dir, prev, or current
                if(ep->d_type==DT_DIR){
                    // dang, is a directory, reccursive sir
                    char *absPath = malloc(sizeof(char)*512);
                    // create {fpath}/ep->d_name absolute path
                    // rename this directory
                    char *newName = renameformatdir(fpath, ep->d_name);
                    // get abs after renamed
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseEncode(absPath);
                }else if(ep->d_type == DT_REG){
                    // just regular type
                    rename_encode_file(fpath, ep->d_name, 0);
                }
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // return NULL;
}

void traverseDecode(char *path){
    DIR *dp;
    struct dirent *ep;
    char *fpath = malloc(sizeof(char)*512);
    // check if relative path is an absolute path (a.k.a include dirpath)
    if(strncmp(path, dirpath, strlen(dirpath))==0){
        strcpy(fpath, path);
    }else{
        // hm possibly not an absolute path, lets make it absolute
        sprintf(fpath, "%s%s",dirpath,path);
        // free(fpath);// this surely wont affect?  nah better comment it for sure
    }
    // Fpath should be a full path sir
    dp = opendir(fpath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                // not a hidden file, dir, prev, or current
                if(ep->d_type==DT_DIR){
                    // dang, is a directory, reccursive sir
                    char *absPath = malloc(sizeof(char)*512);
                    // create {fpath}/ep->d_name absolute path
                    // rename this directory
                    char *newName = unrenameformatdir(fpath, ep->d_name);
                    // get abs after renamed
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseDecode(absPath);
                }else if(ep->d_type == DT_REG){
                    // just regular type, so because ROT13 and ATBASH is melingkar as in encode 2 kali == decode, fine
                    rename_encode_file(fpath, ep->d_name, 1);
                }
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // return NULL;
}
```
Pada kedua fungsi tersebut, pertama untuk melakukan encode adalah bahwa kita mengambil index setelah garis miring dan kemudian untuk index terakhir kita mengambil index sebelum ekstensi file yang kita butuhkan. Selanjutnya kita menggunakan fuse untuk membaca direktori, mendapatkan path file dan meng-encode file.

```bash
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    printf("getattr| path = %s |\n", path);
    sprintf(fpath,"%s%s",dirpath,path);
    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    res = lstat(fpath, stbuf);

    if (res == -1){
        printf("error getattr sir\n");
        return -errno;
    } 
    printf("getattr| res:%d\n", res);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    printf("read | char buf: %c | size_t size: %ld | off_t offset: %ld\n", *buf, size, offset);
    if(strcmp(path,"/") == 0)
    {
        printf("path is: %s\n", path);
        path=dirpath;
        printf("changed to : %s\n", path);
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    (void) fi;
    printf("opening fpath: %s\n", fpath);
    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);
    // strncat(buf, "fine adition\n", 12);
    printf("end of xmp read | res: %d | buf: \n%s\n", res, buf);
    return res;
}
```
Pada fungsi read dan get attribute fuse dilakukan decode terlebih dahulu untuk membaca dan mendapatkan attribute dari file asli.

```bash
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    printf("I am now in the xmp readdir  sir\n");
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);
    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        printf("in de: %s|%d readdir loop\n", de->d_name, de->d_type);
        struct stat st;

        memset(&st, 0, sizeof(st));
        
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        // Do stuff here
        // de->d_name langsung nama file no absolute path
        // d type hmm, 4 == folder?
        // check if d_type is a directory:
        if(isFirstTime==1){
            if(de->d_name[0] != '.'){
                if(de->d_type == DT_DIR){
                    // hm is a folder, 
                    // hoola hoop, i mean loop the loop
                    char *absPath = malloc(sizeof(512));
                    char *newName = renameformatdir(fpath, de->d_name);
                    
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseEncode(absPath);
                }else if(de->d_type == DT_REG){
                    // just a regular folder
                    rename_encode_file(fpath, de->d_name, 0);
                }
            }
            printf("it is first time\n");
        // }else{
            // yes it fork sir
        //     printf("it NOT NOT NOT first time\n");
        }


        res = (filler(buf, de->d_name, &st, 0));
        // printf("res: %d\n", res);
        if(res!=0) break;
    }
    isFirstTime = 2;// so it aint 1
    closedir(dp);
    printf("end of readdir, with res: %d\n", res);
    return 0;
}
```
Pada fungsi readdir, kita akan mengecek apakah suatu direktori memiliki prefix "Animeku_" atau tidak. Kemudian kita lakukan encode.

### Soal 1.b ###
> Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

#### Dokumentasi Source Code ####
Kita menggunakan fuse rename sehingga kita bisa melakukan rename terhadap direktori.
```bash
static int xmp_rename(const char *from, const char *to)
{
    char *full_from=malloc(sizeof(char)*128), *full_to=malloc(sizeof(char)*128);
    // Save dirpath/from to full from
    sprintf(full_from, "%s%s",dirpath,from);
    // Same like above
    sprintf(full_to, "%s%s",dirpath,to);
    
    // to and from is already a relative path
    printf("from: %s |.| %s\n", from, full_from);
    printf("to: %s |.| %s\n", to, full_to);
    // // check if it is a foludorru
    int doDecodeDir = 0;
    if(isDirectory(full_from)){
        // it is from directory so the to must be directory, there fore check if renamed still in format
        int len_to = strlen(to);
        int last_slashLoc = 0;
        for (int i = len_to-1;i>=0;i--){
            char curChar = to[i];
            if(curChar == '/'){
                last_slashLoc = i;
                break;
            }
        }
        char *dirname = malloc(sizeof(char)*256); 
        copyConstSubStr(dirname, to, last_slashLoc, len_to);
        if(strncmp(dirname, "Animeku_", 8)!=0){
            //whoa it NOT  in encode
            doDecodeDir = 1;
        }else{
            //nah chill, just change it bro
            doDecodeDir = 0;
        }
    }

	int res = rename(full_from, full_to);
	if (res == -1){
        printf("error rename");
        return -errno;
    }
    if(doDecodeDir){
        traverseDecode(full_to);
    }

    free(full_from);free(full_to);
	return 0;
}
```
Kemudian, jika kita melakukan rename terhadap file, maka akan secara otomatis ter-encode.

### Soal 1.c ###
> Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

Sama halnya dengan Soal 1.b, dia akan otomatis ter-decode.


### Soal 1.d ###
> Setiap data yang terencode akan masuk dalam file “Wibu.log” 
> Contoh isi: 
> RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
> RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba


Pada fuse rename kita cek terlebih dahulu apakah file di direktori memeiliki prefix "Animeku_". Kemudian digunakan addLog untuk membuat log file.
#### Dokumentasi Source Code ####

```bash
void addLog(char *full_from, char *full_to, int mode){
    char modes[2][32] = {"terenkripsi", "terdecode"};

	FILE *logfile;
	logfile = fopen("/home/sherlock/Wibu.log","a");
	fprintf(logfile, "RENAME %s %s %s\n",modes[mode],full_from,full_to);
	fclose(logfile);
}
```


### Soal 1.e ###
> Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Encoder ini akan diterapkan di semua direktori.


## Nomor 2 ##
> Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 


### Soal 2.a ###
> Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).



#### Dokumentasi Source Code ####

```bash

```


### Soal 2.b ###
> Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

#### Dokumentasi Source Code ####

```bash

```


### Soal 2.c ###
> Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

#### Dokumentasi Source Code ####

```bash

```


### Soal 2.d ###
> Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.


#### Dokumentasi Source Code ####

```bash

```


### Soal 2.e ###
> Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 
> 
> [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC] 
> 
> Keterangan : 
> Level : Level logging
> dd : Tanggal
> mm : Bulan
> yyyy : Tahun
> HH : Jam (dengan format 24 Jam)	MM : Menit
> SS : Detik
> CMD : System call yang terpanggil
> DESC : Informasi dan parameter tambahan 


#### Dokumentasi Source Code ####

```bash

```



## Nomor 3 ##
> Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :

### Soal 3.a ###
> Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial


```bash

```


### Soal 3.b ###
> Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.


```bash

```


### Soal 3.c ###
> Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.


```bash

```


### Soal 3.d ###
> Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).


```bash

```


### Soal 3.e ###
> Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.


```bash

```


## Dokumentasi ##

