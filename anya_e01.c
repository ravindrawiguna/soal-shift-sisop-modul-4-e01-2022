#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#define DI_ENKRIP 0
#define DI_DECODE 1
static  const  char *dirpath = "/home/sherlock/FakeDocument";

// 'boolean' untuk nandain apakah ini pertama kali sistem jalan or no
// Kalau pertama, semua di encode, kalau engga leave as it be
short isFirstTime =1;

// Dict/map for sandi AN a.k.a rot13 for lower case
char ROT13[27] = {'n','o','p','q','r','s','t','u','v','w','x','y','z','a','b','c','d','e','f','g','h','i','j','k','l','m'};
// Dict/map for sandi AZ a.k.a atbash
char ATBASH[27] = {'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'};

void combineTwoString(char dst[], char str1[], char str2[], int isPath){
    strcpy(dst, str1);
    if(isPath){
        int len_str1 = strlen(str1);
        if(dst[len_str1-1] != '/'){
            char ending[3] = "/";
            strncat(dst, ending, strlen(ending));
        }
    }
    strncat(dst, str2, strlen(str2));
}

void addLog(char *full_from, char *full_to, int mode){
    char modes[2][32] = {"terenkripsi", "terdecode"};

	FILE *logfile;
	logfile = fopen("/home/sherlock/Wibu.log","a");
	fprintf(logfile, "RENAME %s %s %s\n",modes[mode],full_from,full_to);
	fclose(logfile);
}



static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    printf("getattr| path = %s |\n", path);
    sprintf(fpath,"%s%s",dirpath,path);
    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    res = lstat(fpath, stbuf);

    if (res == -1){
        printf("error getattr sir\n");
        return -errno;
    } 
    printf("getattr| res:%d\n", res);
    return 0;
}



// got it from my boy QueueUnderflow
int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}
int isDirectory(const char *path) {
   struct stat statbuf;
   if (stat(path, &statbuf) != 0)
       return 0;
   return S_ISDIR(statbuf.st_mode);
}

// will return the thing goes skrak, i mean encoded str
char *get_encoded_str(char *str){
    int len_str = strlen(str);
    // just uh inu cassu
    char *dest = malloc(sizeof(char)*len_str+4); // just for extra room 
    int doEncode = 1;
    int i = 0;
    for(i = 0;i<len_str;i++){
        char cur_char = str[i];
        // check if smoll case
        if(doEncode){
            if(cur_char >='a' && cur_char < 'z'){
                //lower case sir
                int id = cur_char - 'a';
                //encode with rot13 a.k.a sandi an
                dest[i] = ROT13[id];
            }else if(cur_char >='A' && cur_char <='Z'){
                // hoho uppercassu
                int id = cur_char - 'A';
                dest[i] = ATBASH[id];

            }else{
                if(cur_char == '.'){
                    // after this come the extensine or am fail to filter hidden file, any who, we aint encodin stuff after this
                    doEncode = 0;
                }
                
                // random char, just leave it be
                dest[i] = cur_char;
            }
        }else{
            // no encoding, just copy
            dest[i] = cur_char;
        }
    }
    // if last aint null
    char last_copy = dest[i-1];
    if(last_copy != '\0' && last_copy != '\n'){
        // so we add null in dest[id]
        dest[i] = '\0';
    }
    return dest;
}

void copySubStr(char *dest, char *src, int start, int end){
    int id = 0;
    for(int i = start;i<=end;i++){
        dest[id] = src[i];
        id+=1;
    }
    // if last aint null
    char last_copy = dest[id-1];
    if(last_copy != '\0' && last_copy != '\n'){
        // so we add null in dest[id]
        dest[id] = '\0';
    }
}

void copyConstSubStr(char *dest,const char *src, int start, int end){
    int id = 0;
    for(int i = start;i<=end;i++){
        dest[id] = src[i];
        id+=1;
    }
    // if last aint null
    char last_copy = dest[id-1];
    if(last_copy != '\0' && last_copy != '\n'){
        // so we add null in dest[id]
        dest[id] = '\0';
    }
}

char *renameformatdir(char *full_parent, char *dirname){
    printf("rename got: %s|.| %s\n", full_parent, dirname);
    // check if it aint .. or .
    int lendirname = strlen(dirname);
//==================================================================== 
    if(lendirname==0){
        //uh wtf
        return dirname;
    }
    // so len aint 0
    if(dirname[0]=='.'){
        // this should cover .hiddenstuff, ., and ..
        // maam this is a hidden folder or pointer/shortcut to current/ previous directory aint to way renaming this ting
        // printf("GOT . IN INDEX 0");
        return dirname;
    }
//===================================================SHOULD NOT HAPPEN
    // so most likely not a .hidden, . or ..
    // RENAME HERE SIR
    printf("AINT NO HIDDEN OR CUR OR PREV\n");
    // check if it is already formatted
    if(strncmp(dirname, "Animeku_", 8)==0){
        // dang, sama sir
        char *pseudoNewName = malloc(sizeof(char)*256);
        strcpy(pseudoNewName, dirname);
        return pseudoNewName;
    }

    // rename to Anime_{init}
    char *formated = malloc(sizeof(char)*256);
    
    combineTwoString(formated, "Animeku_", dirname, 0);
    printf("format: %s\n", formated);
    char *prevName = malloc(sizeof(char)*256);
    char *finalName = malloc(sizeof(char)*256);
    combineTwoString(prevName, full_parent, dirname, 1);
    combineTwoString(finalName, full_parent, formated, 1);
    printf("prev: %s |.| new: %s\n", prevName, finalName);
    rename(prevName, finalName);
    addLog(prevName, finalName, DI_ENKRIP);
    // free(prevName);free(finalName);
    return formated;
    // free(newName);

    
}

char *unrenameformatdir(char *full_parent, char *dirname){
    printf("rename got: %s|.| %s\n", full_parent, dirname);
    // check if it aint .. or .
    int lendirname = strlen(dirname);
//==================================================================== 
    if(lendirname==0){
        //uh wtf
        return dirname;
    }
    // so len aint 0
    if(dirname[0]=='.'){
        // this should cover .hiddenstuff, ., and ..
        // maam this is a hidden folder or pointer/shortcut to current/ previous directory aint to way renaming this ting
        // printf("GOT . IN INDEX 0");
        return dirname;
    }
//===================================================SHOULD NOT HAPPEN
    // so most likely not a .hidden, . or ..
    // RENAME HERE SIR
    printf("AINT NO HIDDEN OR CUR OR PREV\n");
    // check if it is already formatted
    if(strncmp(dirname, "Animeku_", 8)==0){
        // naa ke format, undo this sir
        // rename to {init}
        char *unformated = malloc(sizeof(char)*256);
        
        copySubStr(unformated, dirname, 8, lendirname);
        
        char *prevName = malloc(sizeof(char)*256);
        char *finalName = malloc(sizeof(char)*256);
        combineTwoString(prevName, full_parent, dirname, 1);
        combineTwoString(finalName, full_parent, unformated, 1);
        printf("prev: %s |.| new: %s\n", prevName, finalName);
        rename(prevName, finalName);
        addLog(prevName, finalName, DI_DECODE);
        // free(prevName);free(finalName);
        return unformated;
        // free(newName);
    }else{
        // dang, udah ga keformat ya wes
        char *pseudoNewName = malloc(sizeof(char)*256);
        strcpy(pseudoNewName, dirname);
        return pseudoNewName;
    }    
}


void rename_encode_file(char * full_parent, char * fileName, int mode){
    char *encoded = get_encoded_str(fileName);
    printf("enc: %s\n", encoded);
    char *prevName = malloc(sizeof(char)*256);
    char *finalName = malloc(sizeof(char)*256);
    combineTwoString(prevName, full_parent, fileName, 1);
    combineTwoString(finalName, full_parent, encoded, 1);
    printf("prev: %s |.| new: %s\n", prevName, finalName);
    rename(prevName, finalName);
    addLog(prevName, finalName, mode);
    // free(encoded);
}



void traverseEncode(char *path){
    DIR *dp;
    struct dirent *ep;
    char *fpath = malloc(sizeof(char)*512);
    // check if relative path is an absolute path (a.k.a include dirpath)
    if(strncmp(path, dirpath, strlen(dirpath))==0){
        strcpy(fpath, path);
    }else{
        // hm possibly not an absolute path, lets make it absolute
        sprintf(fpath, "%s%s",dirpath,path);
        // free(fpath);// this surely wont affect?  nah better comment it for sure
    }
    // Fpath should be a full path sir
    dp = opendir(fpath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                // not a hidden file, dir, prev, or current
                if(ep->d_type==DT_DIR){
                    // dang, is a directory, reccursive sir
                    char *absPath = malloc(sizeof(char)*512);
                    // create {fpath}/ep->d_name absolute path
                    // rename this directory
                    char *newName = renameformatdir(fpath, ep->d_name);
                    // get abs after renamed
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseEncode(absPath);
                }else if(ep->d_type == DT_REG){
                    // just regular type
                    rename_encode_file(fpath, ep->d_name, 0);
                }
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // return NULL;
}

void traverseDecode(char *path){
    DIR *dp;
    struct dirent *ep;
    char *fpath = malloc(sizeof(char)*512);
    // check if relative path is an absolute path (a.k.a include dirpath)
    if(strncmp(path, dirpath, strlen(dirpath))==0){
        strcpy(fpath, path);
    }else{
        // hm possibly not an absolute path, lets make it absolute
        sprintf(fpath, "%s%s",dirpath,path);
        // free(fpath);// this surely wont affect?  nah better comment it for sure
    }
    // Fpath should be a full path sir
    dp = opendir(fpath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(ep->d_name[0] != '.'){
                // not a hidden file, dir, prev, or current
                if(ep->d_type==DT_DIR){
                    // dang, is a directory, reccursive sir
                    char *absPath = malloc(sizeof(char)*512);
                    // create {fpath}/ep->d_name absolute path
                    // rename this directory
                    char *newName = unrenameformatdir(fpath, ep->d_name);
                    // get abs after renamed
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseDecode(absPath);
                }else if(ep->d_type == DT_REG){
                    // just regular type, so because ROT13 and ATBASH is melingkar as in encode 2 kali == decode, fine
                    rename_encode_file(fpath, ep->d_name, 1);
                }
            }
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    // return NULL;
}

static int xmp_rename(const char *from, const char *to)
{
    char *full_from=malloc(sizeof(char)*128), *full_to=malloc(sizeof(char)*128);
    // Save dirpath/from to full from
    sprintf(full_from, "%s%s",dirpath,from);
    // Same like above
    sprintf(full_to, "%s%s",dirpath,to);
    
    // to and from is already a relative path
    printf("from: %s |.| %s\n", from, full_from);
    printf("to: %s |.| %s\n", to, full_to);
    // // check if it is a foludorru
    int doDecodeDir = 0;
    if(isDirectory(full_from)){
        // it is from directory so the to must be directory, there fore check if renamed still in format
        int len_to = strlen(to);
        int last_slashLoc = 0;
        for (int i = len_to-1;i>=0;i--){
            char curChar = to[i];
            if(curChar == '/'){
                last_slashLoc = i;
                break;
            }
        }
        char *dirname = malloc(sizeof(char)*256); 
        copyConstSubStr(dirname, to, last_slashLoc, len_to);
        if(strncmp(dirname, "Animeku_", 8)!=0){
            //whoa it NOT  in encode
            doDecodeDir = 1;
        }else{
            //nah chill, just change it bro
            doDecodeDir = 0;
        }
    }

	int res = rename(full_from, full_to);
	if (res == -1){
        printf("error rename");
        return -errno;
    }
    if(doDecodeDir){
        traverseDecode(full_to);
    }

    free(full_from);free(full_to);
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    printf("I am now in the xmp readdir  sir\n");
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);
    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        printf("in de: %s|%d readdir loop\n", de->d_name, de->d_type);
        struct stat st;

        memset(&st, 0, sizeof(st));
        
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        // Do stuff here
        // de->d_name langsung nama file no absolute path
        // d type hmm, 4 == folder?
        // check if d_type is a directory:
        if(isFirstTime==1){
            if(de->d_name[0] != '.'){
                if(de->d_type == DT_DIR){
                    // hm is a folder, 
                    // hoola hoop, i mean loop the loop
                    char *absPath = malloc(sizeof(512));
                    char *newName = renameformatdir(fpath, de->d_name);
                    
                    combineTwoString(absPath, fpath, newName, 1);
                    traverseEncode(absPath);
                }else if(de->d_type == DT_REG){
                    // just a regular folder
                    rename_encode_file(fpath, de->d_name, 0);
                }
            }
            printf("it is first time\n");
        // }else{
            // yes it fork sir
        //     printf("it NOT NOT NOT first time\n");
        }


        res = (filler(buf, de->d_name, &st, 0));
        // printf("res: %d\n", res);
        if(res!=0) break;
    }
    isFirstTime = 2;// so it aint 1
    closedir(dp);
    printf("end of readdir, with res: %d\n", res);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    printf("read | char buf: %c | size_t size: %ld | off_t offset: %ld\n", *buf, size, offset);
    if(strcmp(path,"/") == 0)
    {
        printf("path is: %s\n", path);
        path=dirpath;
        printf("changed to : %s\n", path);
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    (void) fi;
    printf("opening fpath: %s\n", fpath);
    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);
    // strncat(buf, "fine adition\n", 12);
    printf("end of xmp read | res: %d | buf: \n%s\n", res, buf);
    return res;
}


// void createDir(char directoryName[]){
//   // mengefork to create dir
//   pid_t childAbuse_mkdirId = fork();
//   if(childAbuse_mkdirId < 0){
//     exit(EXIT_FAILURE);
//   }
//   if(childAbuse_mkdirId == 0){
//     // li shi bru, we are in child
//     execlp("mkdir", "mkdir",directoryName, NULL);
//     //here child will die
//   }else{
//     waitChildToDie(); 
//   }
// }


static int xmp_mkdir (const char *path , mode_t mode){
    char fpath[1000];
    printf("I am now in the xmp mkdir  sir\n");
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    printf("after storing dirpath|-|path: %s|-|%s in fpath: %s\n", dirpath, path, fpath);
    int res;
    char *command = malloc(sizeof(char)*128);
    combineTwoString(command, "mkdir ", fpath, 0);
    printf("command: %s\n", command);
    res = system(command);
    // createDir(fpath);
    // res = 0;
    free(command);
    if (res == -1) res = -errno;
   
    printf("end of mkdir\n");
    return res;
}


static struct fuse_operations xmp_oper = {
    .getattr    = xmp_getattr,
    .readdir    = xmp_readdir,
    .read       = xmp_read,
    .mkdir      = xmp_mkdir,
    .rename		= xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    printf("Returning fuse main, with xmp oper\n");
    return fuse_main(argc, argv, &xmp_oper, NULL);
}